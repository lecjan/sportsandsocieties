<?php
    // include connection to mysql database
    include('spsoc_db_conn.php');   
    
    // line important to set temp variable if $_POST operation variable is not set
    $iniOperation = isset($_POST['operation']) ? $_POST['operation'] : "no_insert";
    
    $spsoc_title = isset($_POST['spsoc_title']) ? filter_input(INPUT_POST, 'spsoc_title', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $spsoc_short_desc = isset($_POST['spsoc_short_desc']) ? filter_input(INPUT_POST, 'spsoc_short_desc', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $spsoc_full_desc = isset($_POST['spsoc_full_desc']) ? filter_input(INPUT_POST, 'spsoc_full_desc', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $spsoc_contact_title = isset($_POST['spsoc_contact_title']) ? filter_input(INPUT_POST, 'spsoc_contact_title', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $spsoc_contact_name = isset($_POST['spsoc_contact_name']) ? filter_input(INPUT_POST, 'spsoc_contact_name', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $spsoc_contact_phone = isset($_POST['spsoc_contact_phone']) ? filter_input(INPUT_POST, 'spsoc_contact_phone', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $spsoc_contact_email = isset($_POST['spsoc_contact_email']) ? filter_input(INPUT_POST, 'spsoc_contact_email', FILTER_SANITIZE_SPECIAL_CHARS) : "";       
    
    // adding any new event
    if ($iniOperation == "do_insert") {

        $query = "INSERT INTO sports_societies VALUES('', ?, ?, ?, ?, ?, ?, ?)";
        
        if ($stmt = mysqli_prepare($conn, $query)) {

            /* bind parameters for markers */
            mysqli_stmt_bind_param($stmt, 'sssssss', $spsoc_title, $spsoc_short_desc, $spsoc_full_desc, $spsoc_contact_title, $spsoc_contact_name, $spsoc_contact_phone, $spsoc_contact_email);         

            /* execute statement */
            mysqli_stmt_execute($stmt);

            // display what result was on update
            if (mysqli_affected_rows($conn)>0) {        
                //this will be displayed when the query was successful
                echo "<div>Record was added.</div>";
                echo "<br>";
            } else {
                // and when it fails
                echo "<div>Record hasn't been added";
            }

            /* close statement */
            mysqli_stmt_close($stmt);
        }           
    }
    /* close connection */
    mysqli_close($conn);    
?>


<html>
    <head>
        <title>S&S Content Management | Edit Sport or Society</title>
    </head>
    <body>
        <h2>Dundalk Institute of Technology</h2>
        <h1>Edit Sports and Society Entry</h1>        
        <br>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">

        <br>Complete the form below and press the submit button to add the sports or society</p>
        <fieldset>
            <label><strong>Title:</strong></label><br><input type=text name=spsoc_title size=50 maxlength=50><br>
            <label><strong>Short Description:</strong></label><br><input type=text name=spsoc_short_desc size=50 maxlength=255><br>

            <!--
            <p><strong>Full Description:</strong><br><input type=text name=spsoc_full_desc size=50></p>
            -->

            <label><strong>Full Description:</strong></label><br><textarea name="spsoc_full_desc" rows="10" cols="52"></textarea><br>

            <label><strong>Contact person Title:</strong></label><br><input type=text name=spsoc_contact_title size=25><br>
            <label><strong>Contact person Name:</strong></label><br><input type=text name=spsoc_contact_name size=50><br>
            <label><strong>Contact phone:</strong></label><br><input type=text name=spsoc_contact_phone size=25><br>
            <label><strong>Contact email:</strong></label><br><input type=text name=spsoc_contact_email size=50><br>
        </fieldset>
        <input type="hidden" name = "operation" value = "do_insert">
        <br><br>
        <input type="submit" name="submit" value="Add Sport Club / Society">
    </form>          
        <br><br>
        <a href="spsoc_list_mng.php">Click to return to the Sport and Societies List Management</a> <br>       
    </body>
</html>


