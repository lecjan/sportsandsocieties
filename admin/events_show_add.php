<html>
    <head>
        <title>Show/Add Events</title>
    </head>
    <body>
<?php
    // include connection to mysql database
    include('spsoc_db_conn.php'); 
    
    // first time event_1.php is called it called with $_GET variables
    // we must check are they set and if not set them to default invalid number 0 for day, month, year
    $iniMonth = isset($_GET['m']) ? $_GET['m'] : 0;
    $iniDay = isset($_GET['d']) ? $_GET['d'] : 0;
    $iniYear = isset($_GET['y']) ? $_GET['y'] : 0;   
    
    // because add event form is using a POST method and form action  is script itself
    // on self call variables $_GET are not set so will be all 0 as default
    // therefore we must perform a check is that so
    // and then check form $_POST variables are they set by the form. If not same default are applied
    if ($iniMonth == 0 && $iniDay == 0 && $iniYear == 0) {
        $iniMonth = isset($_POST['m']) ? $_POST['m'] : 0;
        $iniDay = isset($_POST['d']) ? $_POST['d'] : 0;
        $iniYear = isset($_POST['y']) ? $_POST['y'] : 0;           
    }
    
    // line important to set temp variable if $_POST operation variable is not set
    $iniOperation = isset($_POST['operation']) ? $_POST['operation'] : "no_insert";
    
    // adding any new event
    if ($iniOperation == "do_insert") {
        
        // building an event date from sent variables via $_POST method from the form
        $event_date = $_POST['y']."-".$_POST['m']."-".$_POST['d']." ".$_POST['event_time_hh'].":".$_POST['event_time_mm'].":00";
        $event_title = isset($_POST['event_title']) ? filter_input(INPUT_POST, 'event_title', FILTER_SANITIZE_SPECIAL_CHARS) : "";
        $event_short_desc = isset($_POST['event_short_desc']) ? filter_input(INPUT_POST, 'event_short_desc', FILTER_SANITIZE_SPECIAL_CHARS) : "";        
        
        // creating a sql query
        // first value is empty because it is autoincremented id
        $query = "INSERT INTO calendar_events VALUES('', ?, ?, ?)";
        
        if ($stmt = mysqli_prepare($conn, $query)) {

            /* bind parameters for markers */
            mysqli_stmt_bind_param($stmt, 'sss', $event_title, $event_short_desc, $event_date);         

            /* execute statement */
            mysqli_stmt_execute($stmt);

            // display what result was on update
            if (mysqli_affected_rows($conn)>0) {        
                //this will be displayed when the query was successful
                echo "<div>Record was added.</div>";
                echo "<br>";
            } else {
                // and when it fails
                echo "<div>Record hasn't been added";
            }

            /* close statement */
            mysqli_stmt_close($stmt);
        }          
        
    }
    
    
    // showing events for this day    
    
    // prepared sql statement getting event for a given by $_GET date
    // check carefully column names because wrong one cause a query to corrupt whole script
    $query = "SELECT id, event_title, event_short_desc, date_format(event_start, '%l:%i %p') as fmt_date FROM calendar_events WHERE month(event_start) = ? AND dayofmonth(event_start) = ? AND year(event_start) = ? ORDER BY event_start";
 
    $event_txt = "";   
    
    if ($stmt = mysqli_prepare($conn, $query)) {

        /* bind parameters for markers */
        mysqli_stmt_bind_param($stmt, 'sss', $iniMonth, $iniDay, $iniYear);         

        /* execute statement */
        mysqli_stmt_execute($stmt);

        /* bind result variables */
        mysqli_stmt_bind_result($stmt, $id, $event_title, $event_short_desc, $fmt_date);

        
        /* store result must be executed to determine number of rows */
        mysqli_stmt_store_result($stmt);

        //printf("Number of rows: %d.\n", mysqli_stmt_num_rows($stmt));        
        
        if (mysqli_stmt_num_rows($stmt)>0) {        
        
            /* fetch values */
            while (mysqli_stmt_fetch($stmt)) {
                $event_txt .= "<P><strong>$fmt_date</strong>:$event_title<br>$event_short_desc"
                        . "&nbsp;&nbsp;<a href=\"events_details_mng.php?id=$id\">Details</a>";
            }

        }
        /* close statement */
        mysqli_stmt_close($stmt);
    }   
    
    if ($event_txt != "") {
        echo "<P><strong>Today's Events for $iniDay/$iniMonth/$iniYear:</strong>$event_txt<hr noshade width=80%";
    }
    
    /* close connection */
    mysqli_close($conn);    
?>
        
        
        <!-- showing form for adding and event -->
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">

            <p><strong>Would you like to add an event?</strong>
            <br>Complete the form below and press the submit button to add the event and refresh this window.</p>
            <p><strong>Event Title:</strong><br><input type=text name=event_title size=50 maxlength=50>
            <p><strong>Event Description:</strong><br><input type=text name=event_short_desc size=50 maxlength=255>   
            <p><strong>Event Time (hh:mm):</strong><br>

                <select name="event_time_hh">;  

                <?php
                // creating a dropdown list for selection of time hour
                for ($x=1; $x <= 24; $x++) {
                    echo "<option value=\"$x\">$x</option>";
                }
                ?>
                </select> :

            <!-- creating a dropdown list for selection of time minutes -->
            <select name="event_time_mm">
                <option value="00">00</option>
                <option value="15">15</option>
                <option value="30">30</option>
                <option value="45">45</option>        
            </select>
            <input type="hidden" name = "m" value = "<?php echo $iniMonth; ?>">
            <input type="hidden" name = "d" value = "<?php echo $iniDay; ?>">
            <input type="hidden" name = "y" value = "<?php echo $iniYear; ?>">
            <input type="hidden" name = "operation" value = "do_insert">
            <br><br>
            <input type="submit" name="submit" value="Add Event">
        </form>  
        <a href="events_calendar_mng.php">Back to events calendar management</a>
    </body>
</html>