<?php
    // include connection to mysql database
    include('spsoc_db_conn.php');   
    
    $searched_id = isset($_GET['id']) ? filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS) : 0;
    
    // line important to set temp variable if $_POST operation variable is not set
    $iniOperation = isset($_POST['operation']) ? $_POST['operation'] : "no_update";
    
    $event_id = isset($_POST['event_id']) ? filter_input(INPUT_POST, 'event_id', FILTER_SANITIZE_SPECIAL_CHARS) : 0; // setting to 0 when there was no param
    $event_title = isset($_POST['event_title']) ? filter_input(INPUT_POST, 'event_title', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $event_short_desc = isset($_POST['event_short_desc']) ? filter_input(INPUT_POST, 'event_short_desc', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $event_start = isset($_POST['event_start']) ? filter_input(INPUT_POST, 'event_start', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    
    // adding any new event
    if ($iniOperation == "do_update") {

        // if $event_id<>0 then access to update is legitimate otherwise script was called with no params
        if ($event_id <> 0) {

            // if method was POST we must apply this id to $searched_id to be visible for links and select statesment
            // We do that only when from a form POST             
            $searched_id = $event_id;
            
            // creating a sql query
            $query = "UPDATE calendar_events SET event_title=?, event_short_desc=?, event_start=? WHERE id=?";

            if ($stmt = mysqli_prepare($conn, $query)) {

                /* bind parameters for markers */
                mysqli_stmt_bind_param($stmt, 'ssss', $event_title, $event_short_desc, $event_start, $event_id);         

                /* execute statement */
                mysqli_stmt_execute($stmt);

                // display what result was on update
                if (mysqli_affected_rows($conn)>0) {        
                    //this will be displayed when the query was successful
                    echo "<div>Record was updated.</div>";
                    echo "<br>";
                } else {
                    // and when it fails
                    echo "<div>Record hasn't been updated";
                }
                
                /* close statement */
                mysqli_stmt_close($stmt);
            }            
        }
    }    
    
?>

<html>
    <head>
        <title>S&S Content Management | Edit Sport or Society</title>
    </head>
    <body>
        <h2>Dundalk Institute of Technology</h2>
        <h1>Edit Event Entry</h1>        
        <br>
<?php        
        
    // getting the details of the event from database
    
    // preparing the query for statement
    $query = "SELECT * FROM calendar_events WHERE id=?";

    if ($stmt = mysqli_prepare($conn, $query)) {

        /* bind parameters for markers */
        mysqli_stmt_bind_param($stmt, 's', $searched_id);         

        /* execute statement */
        mysqli_stmt_execute($stmt);

        /* bind result variables */
        mysqli_stmt_bind_result($stmt, $id, $event_title, $event_short_desc, $event_start);
                
        /* store result must be executed to determine number of rows */
        mysqli_stmt_store_result($stmt);

        //printf("Number of rows: %d.\n", mysqli_stmt_num_rows($stmt));        
        
        if (mysqli_stmt_num_rows($stmt)>0) {        
        
            // fetching array of results and setting just an event_titles to html text $event_title_txt
            while (mysqli_stmt_fetch($stmt)) {

                // storing unique society id
                $event_id = stripslashes($id);

                // storing clean fields of sport or society
                $event_title = stripslashes($event_title);
                $event_short_desc = stripslashes($event_short_desc);
                $event_start = stripslashes($event_start);
            }

        }
        /* close statement */
        mysqli_stmt_close($stmt);
    }    
    
    /* close connection */
    mysqli_close($conn);    
?>   
        
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">

        <fieldset>
            <label><strong>Title:</strong></label><br><input type="text" name="event_title" size="50" maxlength="50" value="<?php echo $event_title;?>" ><br>
            <label><strong>Short Description:</strong></label><br><input type="text" name="event_short_desc" size="50" maxlength="255" value="<?php echo $event_short_desc;?>" ><br>
            <label><strong>Event Start Date/Time:</strong></label><br><input type="text" name="event_start" size="19" maxlength="19" value="<?php echo $event_start;?>" ><br>
        </fieldset>
        <input type="hidden" name="event_id" value="<?php echo $event_id;?>">
        <input type="hidden" name = "operation" value = "do_update">
        <br><br>
        <input type="submit" name="submit" value="Update Event Details">
    </form>          
        <br><br>
        <a href="events_details_mng.php?id=<?php echo $searched_id; ?>">Back to details...</a>          
    </body>
</html>

