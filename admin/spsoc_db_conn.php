<?php

// for localhost - testing
//$dbhost = "localhost";
//$dbuser = "root";
//$dbpass = "";
//$dbname = "spsoc_db";

// for testing on life server on artifactforge.com/spsoc
$dbhost = "www.artifactforge.com";
$dbuser = "yankovsk_spsoc";
$dbpass = "Spsoc-password";
$dbname = "yankovsk_spsoc_db";

$conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname) or die ('Error connecting to mysql yankovsk_spsoc_db');

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s<br>\n", mysqli_connect_error());
    exit();
}

?>
