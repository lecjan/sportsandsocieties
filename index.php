<?php
?>

<html>
    <head>
        
        <meta charset="utf-8">
        <meta name="description" content="DkIT Sports and Societies">
        <meta name="author" content="Lech Jankowski">        

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        
        <!-- mobile app mode enable -->
        <meta name="mobile-web-app-capable" content="yes">
        
        <!-- mobile app mode enable old version / safari full screen enabled-->
        <meta name="apple-mobile-web-app-capable" content="yes">

        <!-- icons to push -->
        
        <!-- for Windows Phone pin to start -->
        <meta name="application-name" content="DkIT Sports & Societies App" >
        <meta name="msapplication-TileColor" content="#1B262B">
        <meta name="msapplication-square70x70logo" content="windows-smalltile.png" >
        <meta name="msapplication-square150x150logo" content="windows-mediumtile.png" >
        <meta name="msapplication-wide310x150logo" content="windows-widetile.png" >
        <meta name="msapplication-square310x310logo" content="windows-largetile.png" >

        <!-- using manifest.json for pin to homescreen Android/Chrome-->
        <link href="manifest.json" rel="manifest" />
        
        <!-- standard icons push Apple -->
        <link href="apple-touch-icon-57x57.png" rel="apple-touch-icon" />
        <link href="apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76" />
        <link href="apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120" />
        <link href="apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152" />
        <link href="apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180" />
        <!-- startup image apple 320x460px -->
        <link href="apple-touch-startup-image.png" rel="apple-touch-startup-image" />        
        
        <!-- standard icons push Chrome -->
        <link href="icon-hires-196x196.png" rel="shortcut icon" sizes="196x196" />
        <link href="icon-hires-192x192.png" rel="icon" sizes="192x192" />
        <link href="icon-normal-128x128.png" rel="icon" sizes="128x128" />        
        
        <!-- end icons -->        
        
        
        
        <title>DkIT Sport & Societies</title>
        
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/spsoc.css" rel="stylesheet">
        
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">        

        <script src="js/bootstrap.min.js"></script>        
               
    </head>

    <body>
        <div id="wrapper">
        
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">DkIT Sport & Societies</a>
                </div>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">

                    <ul class="nav navbar-nav side-nav">
                        <li><a href="bs_info.php">About</a></li>                        
                        <li><a href="bs_spsoc_list.php">List Sports & Societies</a></li>
                        <li><a href="bs_events_calendar.php">Events Calendar</a></li>
                        <li><a href="bs_social.php">Social Media</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->                    
            </nav>            
            
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="img/DkITS&S_173x100.png" class="img-responsive" alt="DKIT Sports and Societies logo">
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="col-xs-6 col-md-3 col-lg-6">
                            <a href="bs_info.php" class="thumbnail">
                              <img class="img-responsive" src="img/icon_thumbnail_1_300x300_info.png" alt="icon one">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3 col-lg-6">
                            <a href="bs_spsoc_list.php" class="thumbnail">
                              <img class="img-responsive" src="img/icon_thumbnail_2_300x300_list.png" alt="icon two">
                            </a>
                        </div>
                    </div>         

                    <div class="row">
                        <div class="col-xs-6 col-md-3 col-lg-6">
                            <a href="bs_events_calendar.php" class="thumbnail">
                              <img class="img-responsive" src="img/icon_thumbnail_3_300x300_calendar.png" alt="icon three">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3 col-lg-6">
                            <a href="bs_social.php" class="thumbnail">
                              <img class="img-responsive" src="img/icon_thumbnail_4_300x300_social.png" alt="icon four">
                            </a>
                        </div>
                    </div>  
                    

                </div>
            </div>
            <!-- /#page-content-wrapper -->            
            
        </div> <!-- wrapper -->
        
        <!-- jQuery -->
        <script src="js/jquery-1.11.1.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>        
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>
    
    </body>
</html>

