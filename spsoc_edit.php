<?php
    // include connection to mysql database
    include('spsoc_db_conn.php');   
    
    $searched_id = isset($_GET['id']) ? filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS) : 0;
    
    // line important to set temp variable if $_POST operation variable is not set
    $iniOperation = isset($_POST['operation']) ? $_POST['operation'] : "no_update";
    
    $spsoc_id = isset($_POST['spsoc_id']) ? filter_input(INPUT_POST, 'spsoc_id', FILTER_SANITIZE_SPECIAL_CHARS) : 0; // setting to 0 when there was no param
    $spsoc_title = isset($_POST['spsoc_title']) ? filter_input(INPUT_POST, 'spsoc_title', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $spsoc_short_desc = isset($_POST['spsoc_short_desc']) ? filter_input(INPUT_POST, 'spsoc_short_desc', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $spsoc_full_desc = isset($_POST['spsoc_full_desc']) ? filter_input(INPUT_POST, 'spsoc_full_desc', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $spsoc_contact_title = isset($_POST['spsoc_contact_title']) ? filter_input(INPUT_POST, 'spsoc_contact_title', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $spsoc_contact_name = isset($_POST['spsoc_contact_name']) ? filter_input(INPUT_POST, 'spsoc_contact_name', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $spsoc_contact_phone = isset($_POST['spsoc_contact_phone']) ? filter_input(INPUT_POST, 'spsoc_contact_phone', FILTER_SANITIZE_SPECIAL_CHARS) : "";
    $spsoc_contact_email = isset($_POST['spsoc_contact_email']) ? filter_input(INPUT_POST, 'spsoc_contact_email', FILTER_SANITIZE_SPECIAL_CHARS) : "";     
    
    // adding any new event
    if ($iniOperation == "do_update") {

        // if $spsoc_id<>0 then access to update is legitimate otherwise script was called with no params
        if ($spsoc_id <> 0) {

            // if method was POST we must apply this id to $searched_id to be visible for links and select statesment
            // We do that only when from a form POST             
            $searched_id = $spsoc_id;
            
            // creating a sql query
            $query = "UPDATE sports_societies SET spsoc_title=?, spsoc_short_desc=?, spsoc_full_desc=?, spsoc_contact_title=?, spsoc_contact_name=?, spsoc_contact_phone=?, spsoc_contact_email=? WHERE id=?";

            if ($stmt = mysqli_prepare($conn, $query)) {

                /* bind parameters for markers */
                mysqli_stmt_bind_param($stmt, 'ssssssss', $spsoc_title, $spsoc_short_desc, $spsoc_full_desc, $spsoc_contact_title, $spsoc_contact_name, $spsoc_contact_phone, $spsoc_contact_email, $spsoc_id);         

                /* execute statement */
                mysqli_stmt_execute($stmt);

                // display what result was on update
                if (mysqli_affected_rows($conn)>0) {        
                    //this will be displayed when the query was successful
                    echo "<div>Record was updated.</div>";
                    echo "<br>";
                } else {
                    // and when it fails
                    echo "<div>Record hasn't been updated";
                }
                
                /* close statement */
                mysqli_stmt_close($stmt);
            }            
        }
    }    
    
?>

<html>
    <head>
        <title>S&S Content Management | Edit Sport or Society</title>
    </head>
    <body>
        <h2>Dundalk Institute of Technology</h2>
        <h1>Edit Sports and Society Entry</h1>        
        <br>
<?php        
        
    // getting the details of sports or societies from database
    
    // preparing the query for statement
    $query = "SELECT * FROM sports_societies WHERE id=?";

    if ($stmt = mysqli_prepare($conn, $query)) {

        /* bind parameters for markers */
        mysqli_stmt_bind_param($stmt, 's', $searched_id);         

        /* execute statement */
        mysqli_stmt_execute($stmt);

        /* bind result variables */
        mysqli_stmt_bind_result($stmt, $id, $spsoc_title, $spsoc_short_desc, $spsoc_full_desc, $spsoc_contact_title, $spsoc_contact_name, $spsoc_contact_phone, $spsoc_contact_email);
                
        /* store result must be executed to determine number of rows */
        mysqli_stmt_store_result($stmt);

        //printf("Number of rows: %d.\n", mysqli_stmt_num_rows($stmt));        
        
        if (mysqli_stmt_num_rows($stmt)>0) {        
        
            // fetching array of results and setting just an event_titles to html text $event_title_txt
            while (mysqli_stmt_fetch($stmt)) {

                // storing unique society id
                $spsoc_id = stripslashes($id);

                // storing clean fields of sport or society
                $spsoc_title = stripslashes($spsoc_title);
                $spsoc_short_desc = stripslashes($spsoc_short_desc);
                $spsoc_full_desc = stripslashes($spsoc_full_desc);
                $spsoc_contact_title = stripslashes($spsoc_contact_title);
                $spsoc_contact_name = stripslashes($spsoc_contact_name);
                $spsoc_contact_phone = stripslashes($spsoc_contact_phone);
                $spsoc_contact_email = stripslashes($spsoc_contact_email);
            }

        }
        /* close statement */
        mysqli_stmt_close($stmt);
    }    
    
    /* close connection */
    mysqli_close($conn);    
?>   
        
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">

        <fieldset>
            <label><strong>Title:</strong></label><br><input type="text" name="spsoc_title" size="50" maxlength="50" value="<?php echo $spsoc_title;?>" ><br>
            <label><strong>Short Description:</strong></label><br><input type="text" name="spsoc_short_desc" size="50" maxlength="255" value="<?php echo $spsoc_short_desc;?>" ><br>
            <label><strong>Full Description:</strong></label><br><textarea name="spsoc_full_desc" rows="10" cols="52"><?php echo $spsoc_full_desc;?></textarea><br>
            <label><strong>Contact person Title:</strong></label><br><input type="text" name="spsoc_contact_title" size="25" value="<?php echo $spsoc_contact_title;?>" ><br>
            <label><strong>Contact person Name:</strong></label><br><input type="text" name="spsoc_contact_name" size="50" value="<?php echo $spsoc_contact_name;?>" ><br>
            <label><strong>Contact phone:</strong></label><br><input type="text" name="spsoc_contact_phone" size="25" value="<?php echo $spsoc_contact_phone;?>" ><br>
            <label><strong>Contact email:</strong></label><br><input type="text" name="spsoc_contact_email" size="50" value="<?php echo $spsoc_contact_email;?>" ><br>
        </fieldset>
        <input type="hidden" name="spsoc_id" value="<?php echo $spsoc_id;?>">
        <input type="hidden" name = "operation" value = "do_update">
        <br><br>
        <input type="submit" name="submit" value="Update Sport Club / Society Details">
    </form>          
        <br><br>
        <a href="spsoc_details_mng.php?id=<?php echo $searched_id; ?>">Back to details...</a>          
    </body>
</html>

