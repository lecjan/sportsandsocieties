<?php
?>

<html>
    <head>
        
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="DkIT Sports and Societies">
    <meta name="author" content="Lech Jankowski">        
        
        <title>DkIT Sport & Societies: Info</title>
        
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/spsoc.css" rel="stylesheet">
        
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">        

        <script src="js/bootstrap.min.js"></script>        
               
    </head>

    <body>
        <div id="wrapper">
        
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">DkIT Sport & Societies</a>
                </div>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">

                    <ul class="nav navbar-nav side-nav">
                        <li class="active"><a href="bs_info.php">About</a></li>                        
                        <li><a href="bs_spsoc_list.php">List Sports & Societies</a></li>
                        <li><a href="bs_events_calendar.php">Events Calendar</a></li>
                        <li><a href="bs_social.php">Social Media</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->                    
            </nav>            
            
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="img/DkITS&S_300x173.png" class="img-responsive" alt="DKIT Sports and Societies logo">
                            <p>The Sports and Societies Office plays a pivotal role in the life of the Institute assisting students in the set-up, organisation and running of Sports Clubs and Societies.</p>
                            <p>Your time as a student gives you some of the best opportunities you will ever have to indulge your interests and try out new activities. Hundreds of students are involved in sports clubs as diverse as Paintballing, Camogie, Soccer, D.J’ing, Go Karting or Volleyball.</p>
                            <p>Joining a Sports Club, trying out a new activity or getting involved in a society could change your life. For many students it gives the opportunity to:</p>
                            <br>
                            <ul>
                                <li>Meet friends with similar interests</li>
                                <li>Try things you’ve never had a chance to do before</li>
                                <li>Participate at your own level</li>
                                <li>Take a relaxing break from study</li>
                            </ul>
                            <p>For the sports person who wishes to compete competitively to the person with an interest in something less strenuous, DkIT provides limitless opportunities for everyone who wishes to extract whatever they wish out of their pastime. Be it satisfaction for a task completed, enjoyment about something happening, to the chance of success at the pinnacle of your chosen activity.</p>
                            <p>DkIT offers one of the most extensive ranges of clubs and societies in Ireland as well as various student based activity classes during the year.</p>
                        </div>
                    </div>                    
                    <br><br>
                    <center>
                        <a href="index.php"><button type="""button" class="btn btn-default">Home Page</button></a><br><br>
                    </center> 
                </div>
            </div>
            <!-- /#page-content-wrapper -->            
            
        </div> <!-- wrapper -->
        
        <!-- jQuery -->
        <script src="js/jquery-1.11.1.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>       
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>
    
    </body>
</html>

