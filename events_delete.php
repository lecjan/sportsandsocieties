<?php
    // include connection to mysql database
    include('spsoc_db_conn.php');   
    $searched_id = isset($_GET['id']) ? filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS) : 0;
?>

<html>
    <head>
        <title>S&S Content Management | Delete Sport or Society</title>
    </head>
    <body>
        <h2>Dundalk Institute of Technology</h2>
        <h1>Delete Event Entry</h1>        
        <br>


<?php

    //check if an action was set, we use GET this time since we get the action data from the url
    isset($_GET['action']) ? $action=$_GET['action'] : $action="";

    if($action=='delete'){ //if the user clicked ok, run our delete query
        
        $id=$_REQUEST['id'];
        
        // creating a sql query
        $query = "DELETE FROM calendar_events WHERE id=?";
        
        if ($stmt = mysqli_prepare($conn, $query)) {

            /* bind parameters for markers */
            mysqli_stmt_bind_param($stmt, 's', $id);         

            /* execute statement */
            mysqli_stmt_execute($stmt);

            // display what result was on update
            if (mysqli_affected_rows($conn)>0) {        
                //this will be displayed when the query was successful
                echo "<div>Record was deleted.</div>";
                echo "<br>";
                echo "<a href=\"events_calendar_mng.php\">Click to return to the Sport and Societies Events Calendar Management</a>";
            } else {
                // and when it fails
                echo "<div>Record hasn't been deleted";
            }

            /* close statement */
            mysqli_stmt_close($stmt);
        }                 
        
    } else {

        //setting output to null
        $event_title_txt = "";         
        
        // getting the tiltle of sports or societies from database

        // preparing the query
        $query = "SELECT id,event_title, event_short_desc FROM calendar_events WHERE id=?";

        if ($stmt = mysqli_prepare($conn, $query)) {

            /* bind parameters for markers */
            mysqli_stmt_bind_param($stmt, 's', $searched_id);         

            /* execute statement */
            mysqli_stmt_execute($stmt);

            /* bind result variables */
            mysqli_stmt_bind_result($stmt, $id, $event_title, $event_short_desc);

            /* store result must be executed to determine number of rows */
            mysqli_stmt_store_result($stmt);

            //printf("Number of rows: %d.\n", mysqli_stmt_num_rows($stmt));        

            if (mysqli_stmt_num_rows($stmt)>0) {       

                // fetching array of results and setting just an event_titles to html text $event_title_txt
                while (mysqli_stmt_fetch($stmt)) {

                    // storing unique society id
                    $event_id = stripslashes($id);

                    // storing clean fields of sport or society
                    $event_title = stripslashes($event_title);
                    $event_short_desc = stripslashes($event_short_desc);

                    // building up output html list
                    $event_title_txt .= "<h3>$event_title</h3>\n<p>$event_short_desc</p><br>\n";                    
                }

            }
            /* close statement */
            mysqli_stmt_close($stmt);
        }   
        
        echo "<h2>You are about to delete:&nbsp</h2>".$event_title_txt;     
        echo "<a href='#' onclick='delete_event($searched_id);'>Proceed with Deletion...</a>";
        echo "<br>";
        echo "<a href=\"events_details_mng.php?id=$searched_id\">Back to details...</a>";           
    }

    /* close connection */
    mysqli_close($conn);   
?>
  
    <script type='text/javascript'>
        function delete_event( id ){
            //this script helps us to
            var answer = confirm('Are you sure do delete this entry ?');
            if ( answer ){ //if user clicked ok
                //redirect to url with action as delete and id to the record to be deleted
                window.location = 'events_delete.php?action=delete&id=' + id;
            }
        }
    </script>        
        
    </body>
</html>

