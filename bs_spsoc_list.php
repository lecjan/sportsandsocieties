<?php
    // include connection to mysql database
    include('spsoc_db_conn.php');   
?>

<html>
    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="DkIT Sports and Societies">
        <meta name="author" content="Lech Jankowski">        
        
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/spsoc.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">        

        <script src="js/bootstrap.min.js"></script>        
               
        <title>DkIT Sport&Societies List</title>
    </head>

    <body>
        <div id="wrapper">
        
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">DkIT Sport & Societies</a>
                </div>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">

                    <ul class="nav navbar-nav side-nav">
                        <li><a href="bs_info.php">About</a></li>                        
                        <li class="active"><a href="bs_spsoc_list.php">List Sports & Societies</a></li>
                        <li><a href="bs_events_calendar.php">Events Calendar</a></li>
                        <li><a href="bs_social.php">Social Media</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->                    
            </nav>            
            
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">

                            <!-- <h2>Our Sport Clubs and Societies</h2> -->
                            <img src="img/DkITS&S_173x100.png" class="img-responsive" alt="DKIT Sports and Societies logo">
                            
                            <!-- displaying a breadcrumb -->
                            <ul class="breadcrumb">
                                <li><a href="index.php">Home</a></li>
                                <li class="active"><a href="bs_spsoc_list.php">List of Sports and Societies</a></li>
                            </ul>                            

                            
                            <ol type="1" class="list_group">
                            <?php
                                // getting the tiltle of sports or societies from database

                                // preparing the query
                                $query = "SELECT id,spsoc_title,spsoc_short_desc FROM sports_societies ORDER BY spsoc_title";
                                // retrieving the result
                                $result = mysqli_query($conn,$query) or die(mysql_error());

                                // checking if we have any results
                                if (mysqli_num_rows($result)>0) {

                                    //setting output to null
                                    $spsoc_title_txt = "";

                                    // fetching array of results and setting just an event_titles to html text $event_title_txt
                                    while ($spsoc = mysqli_fetch_array($result)) {

                                        // storing unique society id
                                        $spsoc_id = stripslashes($spsoc['id']);

                                        // storing clean name and short desc of sport or society
                                        $spsoc_title = stripslashes($spsoc['spsoc_title']);
                                        $spsoc_short_desc = stripslashes($spsoc['spsoc_short_desc']);

                                        // building up output html list
                                        $spsoc_title_txt .= "<li class=\"list-group-item\"><strong>$spsoc_title</strong><br>$spsoc_short_desc &nbsp;&nbsp;<br><br><a href=\"bs_spsoc_details.php?id=$spsoc_id\"><span><button type=\"button\" class=\"btn btn-info\" style=\"float: right;\">View Details</button></span><br><br></a></li><br>\n";
                                    }
                                }    
                                echo $spsoc_title_txt;

                                // close connection
                                mysqli_close($conn);
                            ?>
                            </ol>
                            <br><br>
                            <center>
                                <a href="index.php"><button type="""button" class="btn btn-default">Home Page</button></a><br><br>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->            
            
        </div> <!-- wrapper -->
        
        <!-- scripts -->
        
        <!-- jQuery -->
        <script src="js/jquery-1.11.1.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>      
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>
    
    </body>
</html>

