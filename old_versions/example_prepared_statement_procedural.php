<?php
    // include connection to mysql database returns $conn
    include('spsoc_db_conn.php');  

    $input_id = "1";
    $input_title = "DkIT ACM Student Chapter";

    $query = "SELECT id, spsoc_title FROM sports_societies WHERE id>=? AND spsoc_title=?";

    if ($stmt = mysqli_prepare($conn, $query)) {

        /* bind parameters for markers */
        mysqli_stmt_bind_param($stmt, 'ss', $input_id, $input_title);         

        /* execute statement */
        mysqli_stmt_execute($stmt);

        /* bind result variables */
        mysqli_stmt_bind_result($stmt, $id, $spsoc_title);

        
        /* store result must be executed to determine number of rows */
        mysqli_stmt_store_result($stmt);

        //printf("Number of rows: %d.\n", mysqli_stmt_num_rows($stmt));        
        
        if (mysqli_stmt_num_rows($stmt)>0) {        
        
            /* fetch values */
            while (mysqli_stmt_fetch($stmt)) {
                printf ("%s is an society: %s<br>\n", $id, $spsoc_title);
            }

        }
        /* close statement */
        mysqli_stmt_close($stmt);
    }

    /* close connection */
    mysqli_close($conn);
?>