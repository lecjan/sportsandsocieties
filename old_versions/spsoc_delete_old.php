<?php
    // include connection to mysql database
    include('spsoc_db_conn.php');   
    $searched_id = isset($_GET['id']) ? filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS) : 0;
?>

<html>
    <head>
        <title>S&S Content Management | Delete Sport or Society</title>
    </head>
    <body>
        <h2>Dundalk Institute of Technology</h2>
        <h1>Delete Sports and Society Entry</h1>        
        <br>


<?php

    //check if an action was set, we use GET this time since we get the action data from the url
    isset($_GET['action']) ? $action=$_GET['action'] : $action="";

    if($action=='delete'){ //if the user clicked ok, run our delete query
        
        $id=$_REQUEST['id'];
        
        $query = mysqli_query($conn,"DELETE FROM sports_societies WHERE id='$id'") or die(mysqli_error($conn));
        if($query){
            //this will be displayed when the query was successful
            echo "<div>Record was deleted.</div>";
            echo "<br>";
            
            echo "<a href=\"spsoc_list_mng.php\">Click to return to the Sport and Societies List Management</a>";
            
        }
    } else {

        // getting the tiltle of sports or societies from database

        // preparing the query
        $chkSpsoc = "SELECT id,spsoc_title FROM sports_societies WHERE id='$searched_id'";
        // retrieving the result
        $chkSpsoc_res = mysqli_query($conn,$chkSpsoc) or die(mysql_error());

        // checking if we have any results
        if (mysqli_num_rows($chkSpsoc_res)>0) {

            //setting output to null
            $spsoc_title_txt = "";

            // fetching array of results and setting just an event_titles to html text $event_title_txt
            while ($spsoc = mysqli_fetch_array($chkSpsoc_res)) {

                // storing unique society id
                $spsoc_id = stripslashes($spsoc['id']);

                // storing clean name and short desc of sport or society
                $spsoc_title = stripslashes($spsoc['spsoc_title']);

                // building up output html list
                $spsoc_title_txt .= "<strong>$spsoc_title</strong>&nbsp;&nbsp;<br>\n";
            }
        }    
        echo "<h2>You are about to delete:&nbsp<strong>".$spsoc_title_txt."</strong></h2>";     
        echo "<a href='#' onclick='delete_spsoc($searched_id);'>Proceed with Deletion...</a>";
        echo "<br>";
        echo "<a href=\"spsoc_details_mng.php?id=$searched_id\">Back to details...</a>";           
        
    }

?>
    <!--    
    <a href='#' onclick='delete_spsoc(<?php echo $searched_id; ?>);'>Proceed with Deletion...</a>        
    -->
        
    <script type='text/javascript'>
        function delete_spsoc( id ){
            //this script helps us to
            var answer = confirm('Are you sure do delete this entry ?');
            if ( answer ){ //if user clicked ok
                //redirect to url with action as delete and id to the record to be deleted
                window.location = 'spsoc_delete.php?action=delete&id=' + id;
            }
        }
    </script>        
        
    </body>
</html>

