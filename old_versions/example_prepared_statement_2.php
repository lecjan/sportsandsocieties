<?php

$mysqli = new mysqli("localhost", "root", "", "showcalendar");

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$input_id = "1";
$input_title = "DkIT ACM Student Chapter";

/* create a prepared statement */
if ($stmt = $mysqli->prepare("SELECT id, spsoc_title FROM sports_societies WHERE id>=? AND spsoc_title=?")) {

    /* bind parameters for markers */
    $stmt->bind_param('ss', $input_id, $input_title);       

    /* execute query */
    $stmt->execute();

    /* bind result variables */
    $stmt->bind_result($id, $spsoc_title);

    /* fetch value */
    //$stmt->fetch();
    //printf("%s is an society: %s\n", $id, $spsoc_title);

    
    /* fetch values */
    while ($stmt->fetch()) {
        printf ("%s is an society: %s<br>\n", $id, $spsoc_title);
    }    


    /* close statement */
    $stmt->close();
}

/* close connection */
$mysqli->close();
?>