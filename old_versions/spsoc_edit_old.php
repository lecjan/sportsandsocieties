<?php
    // include connection to mysql database
    include('spsoc_db_conn.php');   
    
    
    // $tmp_id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
    
    $searched_id = isset($_GET['id']) ? filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS) : 0;

    //$searched_id = isset($_GET['id']) ? $_GET['id'] : 0;
    
    // line important to set temp variable if $_POST operation variable is not set
    $iniOperation = isset($_POST['operation']) ? $_POST['operation'] : "no_update";
    
    // adding any new event
    if ($iniOperation == "do_update") {
        
        $spsoc_id = $_POST['spsoc_id'];
        $spsoc_title = $_POST['spsoc_title'];
        $spsoc_short_desc = $_POST['spsoc_short_desc'];
        $spsoc_full_desc = $_POST['spsoc_full_desc'];
        $spsoc_contact_title = $_POST['spsoc_contact_title'];
        $spsoc_contact_name = $_POST['spsoc_contact_name'];
        $spsoc_contact_phone = $_POST['spsoc_contact_phone'];
        $spsoc_contact_email = $_POST['spsoc_contact_email'];        
        
        // creating a sql query
        $updSociety = "UPDATE sports_societies SET spsoc_title='$spsoc_title', spsoc_short_desc='$spsoc_short_desc', spsoc_full_desc='$spsoc_full_desc', spsoc_contact_title='$spsoc_contact_title', spsoc_contact_name='$spsoc_contact_name', spsoc_contact_phone='$spsoc_contact_phone', spsoc_contact_email='$spsoc_contact_email' WHERE id='$spsoc_id'";

        // executing updating query
        $query = mysqli_query($conn,$updSociety) or die(mysql_error());

        if($query){
            //this will be displayed when the query was successful
            echo "<div>Record was updated.</div>";
            echo "<br>";
        } else {
            // and when it fails
            echo "<div>Record hasn't been updated";
        }
        
        
    }    
    
?>

<html>
    <head>
        <title>S&S Content Management | Edit Sport or Society</title>
    </head>
    <body>
        <h2>Dundalk Institute of Technology</h2>
        <h1>Edit Sports and Society Entry</h1>        
        <br>
<?php        
        
    // getting the details of sports or societies from database
    
    // preparing the query
    $chkSpsocDetails = "SELECT * FROM sports_societies WHERE id='$searched_id'";
    // retrieving the result
    $chkSpsocDetails_res = mysqli_query($conn,$chkSpsocDetails) or die(mysql_error());

    // checking if we have any results
    if (mysqli_num_rows($chkSpsocDetails_res)>0) {
        
        //setting output to null
        $spsoc_output_txt = "";
        
        // setting initial value of sports or society id. It must be used outside the loop to link to a proper page
        //$spsoc_id = 0;
        
        // fetching array of results and setting just an event_titles to html text $event_title_txt
        while ($spsoc = mysqli_fetch_array($chkSpsocDetails_res)) {
            
            // storing unique society id
            $spsoc_id = stripslashes($spsoc['id']);
            
            // storing clean fields of sport or society
            $spsoc_title = stripslashes($spsoc['spsoc_title']);
            $spsoc_short_desc = stripslashes($spsoc['spsoc_short_desc']);
            $spsoc_full_desc = stripslashes($spsoc['spsoc_full_desc']);
            $spsoc_contact_title = stripslashes($spsoc['spsoc_contact_title']);
            $spsoc_contact_name = stripslashes($spsoc['spsoc_contact_name']);
            $spsoc_contact_phone = stripslashes($spsoc['spsoc_contact_phone']);
            $spsoc_contact_email = stripslashes($spsoc['spsoc_contact_email']);
        }
    }         
?>   
        
        <!-- delete this when produced -->
        <p><strong>Name of society&nbsp;=&nbsp;<?php echo $spsoc_title; ?></strong></p>
        
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">

        <br>Complete the form below and press the submit button to add the sports or society</p>
        <fieldset>
            <label><strong>Title:</strong></label><br><input type="text" name="spsoc_title" size="50" maxlength="50" value="<?php echo $spsoc_title;?>" ><br>
            <label><strong>Short Description:</strong></label><br><input type="text" name="spsoc_short_desc" size="50" maxlength="255" value="<?php echo $spsoc_short_desc;?>" ><br>
            <label><strong>Full Description:</strong></label><br><textarea name="spsoc_full_desc" rows="10" cols="52"><?php echo $spsoc_full_desc;?></textarea><br>
            <label><strong>Contact person Title:</strong></label><br><input type="text" name="spsoc_contact_title" size="25" value="<?php echo $spsoc_contact_title;?>" ><br>
            <label><strong>Contact person Name:</strong></label><br><input type="text" name="spsoc_contact_name" size="50" value="<?php echo $spsoc_contact_name;?>" ><br>
            <label><strong>Contact phone:</strong></label><br><input type="text" name="spsoc_contact_phone" size="25" value="<?php echo $spsoc_contact_phone;?>" ><br>
            <label><strong>Contact email:</strong></label><br><input type="text" name="spsoc_contact_email" size="50" value="<?php echo $spsoc_contact_email;?>" ><br>
        </fieldset>
        <input type="hidden" name="spsoc_id" value="<?php echo $spsoc_id;?>">
        <input type="hidden" name = "operation" value = "do_update">
        <br><br>
        <input type="submit" name="submit" value="Update Sport Club / Society Details">
    </form>          
        <br><br>
        <a href="spsoc_details_mng.php?id=<?php echo $searched_id; ?>">Back to details...</a>          
    </body>
</html>

