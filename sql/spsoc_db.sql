create database if not exists spsoc_db;
use spsoc_db;

create table if not exists calendar_events (
    id int not null primary key auto_increment,
    event_title varchar(50),
    event_short_desc varchar(255),
    event_start datetime
);

create table if not exists sports_societies (
    id int not null primary key auto_increment,
    spsoc_title varchar(50),
    spsoc_short_desc varchar(255),
    spsoc_full_desc text,
    spsoc_contact_title varchar(25),
    spsoc_contact_name varchar(50),
    spsoc_contact_phone varchar(25),
    spsoc_contact_email varchar(50)
);

INSERT INTO `calendar_events` (`id`, `event_title`, `event_short_desc`, `event_start`) VALUES
(1, 'event1', 'event one', '2015-06-18 00:00:00'),
(2, 'event 2', 'event two', '2015-06-29 00:00:00'),
(18, 'Campus Charity Park Run', 'Charity Fund Raising Run for Cancer abcdefghijkl', '2015-07-19 10:00:00'),
(12, 'Celebrating ACM Student Chapter Anniversary 2015  ', 'Event Four Description', '2015-07-19 14:00:00'),
(13, 'Event 5', 'Event Five Description', '2015-06-18 17:45:00'),
(15, 'event 6', 'Event Six', '2015-07-14 13:15:00'),
(16, 'Event 7', 'Event Seven', '2015-06-29 18:15:00'),
(17, 'Event 8', 'Aprils Fools', '2017-04-01 12:30:00');

INSERT INTO `sports_societies` (`id`, `spsoc_title`, `spsoc_short_desc`, `spsoc_full_desc`, `spsoc_contact_title`, `spsoc_contact_name`, `spsoc_contact_phone`, `spsoc_contact_email`) VALUES
(1, 'DkIT ACM Student Chapter', 'Dundalk Institute of Technology Association for Computer Machinery Student Chapter', 'Student Chapter for International Computing Professional Organisation ACM', 'Chairman', 'Lech Jankowski', '0857870045', 'chair@dkit.hotsting.acm.org'),
(2, 'Archery', 'Archery Sports Society', 'Archery Sports Society offers bla bla bla', 'secretary', 'John Smith', '086 123 45 67', 'archery@dkit.ie');
