<?php
    // include connection to mysql database
    include('spsoc_db_conn.php');   
?>

<html>
    <head>
        <title>Sports & Societies List</title>
    </head>
    <body>
        <h2>Dundalk Institute of Technology</h2>
        <h1>Sports and Societies List Management</h1>
        <ol type="1">
<?php
    // getting the tiltle and short description of sports or societies from database
    
    // preparing the query
    $query = "SELECT id,spsoc_title,spsoc_short_desc FROM sports_societies ORDER BY spsoc_title";
    // retrieving the result
    $result = mysqli_query($conn,$query) or die(mysql_error());

    // checking if we have any results
    if (mysqli_num_rows($result)>0) {
        
        //setting output to null
        $spsoc_title_txt = "";
        
        // fetching array of results and setting just an event_titles to html text $event_title_txt
        while ($spsoc = mysqli_fetch_array($result)) {
            
            // storing unique society id
            $spsoc_id = stripslashes($spsoc['id']);
            
            // storing clean name and short desc of sport or society
            $spsoc_title = stripslashes($spsoc['spsoc_title']);
            $spsoc_short_desc = stripslashes($spsoc['spsoc_short_desc']);
            
            // building up output html list
            $spsoc_title_txt .= "<li><strong>$spsoc_title</strong><br>$spsoc_short_desc &nbsp;&nbsp;"
                    . "<a href=\"spsoc_details_mng.php?id=$spsoc_id\">Details</a>"
                    . "</li><br>\n";
        }
    }    
    echo $spsoc_title_txt;
    /* close connection */
    mysqli_close($conn);  
    
?>
        </ol>
        <br><br>
        <a href="spsoc_add.php">Add new Sport Club or Society</a>
        <br><br>    
        <a href="menu_cms.php">Click to return to the Sport and Societies Management Menu</a> <br>        
    </body>
</html>