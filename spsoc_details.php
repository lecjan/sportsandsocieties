<?php
    // include connection to mysql database
    include('spsoc_db_conn.php');   
    $searched_id = isset($_GET['id']) ? filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS) : 0;    
?>

<html>
    <head>
        <title>Sports & Societies Details</title>
    </head>
    <body>
        <h2>Dundalk Institute of Technology</h2>
        <h2>Sport & Societies Details for:</h2>

<?php

    //setting output to null
    $spsoc_output_txt = "";

    // getting the tiltle of sports or societies from database
    
    // preparing the query
    $query = "SELECT * FROM sports_societies WHERE id=?";    
    
    if ($stmt = mysqli_prepare($conn, $query)) {

        /* bind parameters for markers */
        mysqli_stmt_bind_param($stmt, 's', $searched_id);         

        /* execute statement */
        mysqli_stmt_execute($stmt);

        /* bind result variables */
        mysqli_stmt_bind_result($stmt, $id, $spsoc_title, $spsoc_short_desc, $spsoc_full_desc, $spsoc_contact_title, $spsoc_contact_name, $spsoc_contact_phone, $spsoc_contact_email);
                
        /* store result must be executed to determine number of rows */
        mysqli_stmt_store_result($stmt);
        
        if (mysqli_stmt_num_rows($stmt)>0) {        



            // setting initial value of sports or society id. It must be used outside the loop to link to a proper page
            $spsoc_id = 0;
            
            // fetching array of results and setting just an event_titles to html text $event_title_txt
            while (mysqli_stmt_fetch($stmt)) {

                // storing unique society id
                $spsoc_id = stripslashes($id);

                // storing clean fields of sport or society
                $spsoc_title = stripslashes($spsoc_title);
                $spsoc_short_desc = stripslashes($spsoc_short_desc);
                $spsoc_full_desc = stripslashes($spsoc_full_desc);
                $spsoc_contact_title = stripslashes($spsoc_contact_title);
                $spsoc_contact_name = stripslashes($spsoc_contact_name);
                $spsoc_contact_phone = stripslashes($spsoc_contact_phone);
                $spsoc_contact_email = stripslashes($spsoc_contact_email);

                // building up output html list
                $spsoc_output_txt .= "<h2><strong>$spsoc_title</strong></h2><br><br>\n"
                        . "<p><strong>Short description:&nbsp;</strong>$spsoc_short_desc</p>"
                        . "<p><strong>Contact Title:&nbsp;</strong>$spsoc_contact_title</p>"
                        . "<p><strong>Contact Name:&nbsp;</strong>$spsoc_contact_name</p>"
                        . "<p><strong>Contact Phone:&nbsp;</strong>$spsoc_contact_phone</p>"
                        . "<p><strong>Contact Email:&nbsp;</strong>$spsoc_contact_email</p>"
                        . "<p><strong>Description:&nbsp;</strong>$spsoc_full_desc</p>";
                }
        }
        /* close statement */
        mysqli_stmt_close($stmt);
    }      
   
    echo $spsoc_output_txt;

    /* close connection */
    mysqli_close($conn);     
    
?>
        <br><br>

        <a href="spsoc_list.php">Click to return to the Sport and Societies List</a> <br>
    </body>
</html>