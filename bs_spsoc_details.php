<?php
    // include connection to mysql database
    include('spsoc_db_conn.php');   
    $searched_id = isset($_GET['id']) ? filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS) : 0;    
?>

<html>
    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="DkIT Sports and Societies">
        <meta name="author" content="Lech Jankowski">        
        
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/spsoc.css" rel="stylesheet">
        
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">        

        <script src="js/bootstrap.min.js"></script>        
        
        <title>DkIT Sport&Societies Details</title>               
    </head>

    <body>
        <div id="wrapper">
        
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">DkIT Sport & Societies</a>
                </div>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">

                    <ul class="nav navbar-nav side-nav">
                        <li><a href="bs_info.php">About</a></li>                        
                        <li><a href="bs_spsoc_list.php">List Sports & Societies</a></li>
                        <li><a href="bs_events_calendar.php">Events Calendar</a></li>
                        <li><a href="bs_social.php">Social Media</a></li>                        
                    </ul>
                </div>
                <!-- /.navbar-collapse -->                    
            </nav>            
            
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                           
                            <!-- <h2>Details</h2>    -->
                            <img src="img/DkITS&S_173x100.png" class="img-responsive" alt="DKIT Sports and Societies logo">
                            
                            <!-- displaying a breadcrumb -->
                            <ul class="breadcrumb">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="bs_spsoc_list.php">List of Sports and Societies</a></li>
                                <li class="active">Details</li>
                            </ul> 
                       

                            <?php

                                //setting output to null
                                $spsoc_output_txt = "";

                                // getting the tiltle of sports or societies from database

                                // preparing the query
                                $query = "SELECT * FROM sports_societies WHERE id=?";    

                                if ($stmt = mysqli_prepare($conn, $query)) {

                                    /* bind parameters for markers */
                                    mysqli_stmt_bind_param($stmt, 's', $searched_id);         

                                    /* execute statement */
                                    mysqli_stmt_execute($stmt);

                                    /* bind result variables */
                                    mysqli_stmt_bind_result($stmt, $id, $spsoc_title, $spsoc_short_desc, $spsoc_full_desc, $spsoc_contact_title, $spsoc_contact_name, $spsoc_contact_phone, $spsoc_contact_email);

                                    /* store result must be executed to determine number of rows */
                                    mysqli_stmt_store_result($stmt);

                                    if (mysqli_stmt_num_rows($stmt)>0) {        



                                        // setting initial value of sports or society id. It must be used outside the loop to link to a proper page
                                        $spsoc_id = 0;

                                        // fetching array of results and setting just an event_titles to html text $event_title_txt
                                        while (mysqli_stmt_fetch($stmt)) {

                                            // storing unique society id
                                            $spsoc_id = stripslashes($id);

                                            // storing clean fields of sport or society
                                            $spsoc_title = stripslashes($spsoc_title);
                                            $spsoc_short_desc = stripslashes($spsoc_short_desc);
                                            $spsoc_full_desc = stripslashes($spsoc_full_desc);
                                            $spsoc_contact_title = stripslashes($spsoc_contact_title);
                                            $spsoc_contact_name = stripslashes($spsoc_contact_name);
                                            $spsoc_contact_phone = stripslashes($spsoc_contact_phone);
                                            $spsoc_contact_email = stripslashes($spsoc_contact_email);

                                            // building up output html list
                                            $spsoc_output_txt .= "<h2><strong>$spsoc_title</strong></h2><br><br>\n"
                                                    . "<p><strong>Short description:&nbsp;</strong>$spsoc_short_desc</p>"
                                                         . "<p><strong>Contact Title    :&nbsp;</strong>$spsoc_contact_title</p>"
                                                         . "<p><strong>Contact Name     :&nbsp;</strong>$spsoc_contact_name</p>"
                                                         . "<p><strong>Contact Phone    :&nbsp;</strong>$spsoc_contact_phone</p>"
                                                         . "<p><strong>Contact Email    :&nbsp;</strong>$spsoc_contact_email</p>"
                                                         . "<p><strong>Description      :&nbsp;</strong>$spsoc_full_desc</p>";
                                            }
                                    }
                                    /* close statement */
                                    mysqli_stmt_close($stmt);
                                }      

                                echo $spsoc_output_txt;

                                /* close connection */
                                mysqli_close($conn);     

                            ?>
                            <br><br>

                            <a href="bs_spsoc_list.php"><button type="""button" class="btn btn-default">Sport and Societies List</button></a> <br><br>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->            
            
        </div> <!-- wrapper -->

        <!-- scripts -->
                
        <!-- jQuery -->
        <script src="js/jquery-1.11.1.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>     
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>
    
    </body>
</html>

