<?php

$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$dbname = "spsoc_db";

$conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname) or die ('Error connecting to mysql showcalendar');

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s<br>\n", mysqli_connect_error());
    exit();
}

?>
