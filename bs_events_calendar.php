<?php

// for testing only example of an event 25th June 2015
//$eventDate = mktime(12, 0, 0, 6, 25, 2015);
//$eventDayArray = getdate($eventDate);

// resetting output built html used below in results for the day with events
$event_title_txt = "";

include('spsoc_db_conn.php');

// defining constant variable for day (in seconds)
define("ADAY", 60*60*24);

// there was a need to check if $_POST is set
// otherwise PHP returned error of undefined variable
// We are setting initial variables to $_POST if are set
// and to invalid month and year values of 0 for later chackdate()
// if $_POST is not set
$iniMonth = isset($_POST['month']) ? $_POST['month'] : 0;
$iniYear = isset($_POST['year']) ? $_POST['year'] : 0;

// checking was date given by the form or empty
// by validating the date using checkdate
// If date is not set it is not validated,
// if is given and set by the form it will be valid,
// then:
if (!checkdate($iniMonth, 1, $iniYear)) {
    // date is not valid then set date as and array of current day
    $nowArray = getdate();
    // set $month and $year to current one using above array
    $month = $nowArray['mon'];
    $year = $nowArray['year'];
} else {
    // date is set by form then store month and year from $_POST variables
    $month = $iniMonth;
    $year = $iniYear;
}
// using set month and year we make a "start" time & date
// to 00:00:00 MM 01 YY
$start = mktime(12, 0, 0, $month, 1, $year);
// we must retrieve proper array of date/time from start date
$firstDayArray = getdate($start);
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="DkIT Sports and Societies">
        <meta name="author" content="Lech Jankowski">
        
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/spsoc.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">        

        <script src="js/bootstrap.min.js"></script>  

        <title>DkIT Sports&Societies <?php echo "Calendar:".$firstDayArray['month']." ".$firstDayArray['year'] ?></title>        
    </head>
    
    <body>
        <div id="wrapper">
        
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">DkIT Sport & Societies</a>
                </div>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">

                    <ul class="nav navbar-nav side-nav">
                        <li><a href="bs_info.php">About</a></li>                        
                        <li><a href="bs_spsoc_list.php">List Sports & Societies</a></li>
                        <li class="active"><a href="bs_events_calendar.php">Events Calendar</a></li>
                        <li><a href="bs_social.php">Social Media</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->                    
            </nav>  
            
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">         
                            <img src="img/DkITS&S_173x100.png" class="img-responsive" alt="DKIT Sports and Societies logo">
                            
                            <!-- displaying a breadcrumb -->
                            <ul class="breadcrumb">
                                <li><a href="index.php">Home</a></li>
                                <li class="active"><a href="bs_events_calendar.php">Calendar of events<?php echo ": ".$firstDayArray['month']." ".$firstDayArray['year'] ?></a></li>
                                
                            </ul>    
                        </div>
                    </div>    


  

                    <div class="row">
                        <div class="col-lg-12">  
                            <!-- calendar table -->
                            <center>
                            <?php
                                // defining array of days of the week names
                                $days = Array("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun");
                                // creating a table

                                //echo "<table border=1 cellpadding=5<tr>\n";
                                echo "<table class=\"table table-bordered\" style=\"font-size:0.7em;\" cellpadding=5<tr>\n";
                                // creating table cell with names of days of the week
                                foreach ($days as $day) {
                                    echo "<td bgcolor=\"#CCCCCC\" align=center><strong>$day</strong></td>\n";
                                }

                                // building calendar

                                for ($count=0; $count < (6*7); $count++) {

                                    // first day of the month stored into a date array
                                    $dayArray = getDate($start);

                                    // building rows

                                    if (($count % 7) == 0) {
                                        // if 7th day of the week
                                        if ($dayArray['mon'] != $month) {
                                            // if we landed into new month
                                            break;
                                        } else {
                                            // if this is new week close and reopen tanle row
                                            echo "</tr><tr>\n";
                                        }
                                    }

                                    // building table cells / colums
                                    // if $count is lesser than day of the week
                                    // or landed in new month

                                    // $count+1 is set due to a need to start a week from Mon not Sun
                                    // 
                                    if ($count+1 < $firstDayArray['wday'] || $dayArray['mon'] != $month) {
                                        // create an empty cell
                                        echo "<td>&nbsp;</td>\n";
                                    } else {

                                        // getting the events from database for current date
                                        $query = "SELECT event_title FROM calendar_events WHERE month(event_start) = ? AND dayofmonth(event_start) = ? AND year(event_start) = ? ORDER BY event_start";

                                        // resetting variable used later for conditional formatting
                                        $dayHasEvent = false;                    

                                        if ($stmt = mysqli_prepare($conn, $query)) {

                                            /* bind parameters for markers */
                                            mysqli_stmt_bind_param($stmt, 'sss', $month, $dayArray['mday'], $year);         

                                            /* execute statement */
                                            mysqli_stmt_execute($stmt);

                                            /* bind result variables */
                                            mysqli_stmt_bind_result($stmt, $event_title);

                                            /* store result must be executed to determine number of rows */
                                            mysqli_stmt_store_result($stmt);

                                            //printf("Number of rows: %d.\n", mysqli_stmt_num_rows($stmt));        

                                            if (mysqli_stmt_num_rows($stmt)>0) {        

                                                // setting variable to tell later that day has an event
                                                $dayHasEvent = true;                            

                                                /* fetch values */
                                                while (mysqli_stmt_fetch($stmt)) {
                                                    $event_title = stripslashes($event_title);
                                                    //$event_title_txt .= "$event_title <br>";
                                                }

                                            }
                                            /* close statement */
                                            mysqli_stmt_close($stmt);
                                        }                    

                                        if ($dayHasEvent) {
                                            // create a cell with a link to an even.php script to display the event
                                            // We pass through parameters via $_GET variables d,m,y

                                            echo "<td bgcolor=\"#DDDDDD\"><strong>"
                                            . "<a href=\"bs_events_show.php?m=".$month."&d=".$dayArray['mday']."&y=$year\">".$dayArray['mday']." &nbsp;&nbsp; </a>"
                                            . "</strong><br><br>$event_title_txt</td>\n";

                                            //echo "<td bgcolor=\"#DDDDDD\"><strong>"
                                            //. "<a href=\"javascript:eventWindow('event_show_add.php?m=".$month."&d=".$dayArray['mday']."&y=$year');\">".$dayArray['mday']." &nbsp;&nbsp; </a>"
                                            //. "</strong><br><br>$event_title_txt</td>\n";

                                        } else {
                                            // common day - no event
                                            echo "<td>".$dayArray['mday']." &nbsp;&nbsp; <br><br></td>\n";                        
                                        }

                                        // removing variable $event_title_txt to prevent from occuring in different dates
                                        // here was but caused undefined variable error: unset($event_title_txt);
                                        $event_title_txt = "";

                                        // increment start date variable
                                        $start += ADAY;
                                    }                
                                }
                                echo "</tr></table>";

                                /* close connection */
                                mysqli_close($conn);            
                            ?>
                            </center>

                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">         
                            <!-- form for month and year selection -->
                            <center>
                            <h6><strong>Select a Month/Year Combination</strong></h6>
                            <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                <select name="month">
                                    <?php
                                        $months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                                        for ($x=1; $x <= count($months); $x++) {
                                            // setting value to month number -  see escape chars for dbl quotes
                                            echo "<option value=\"$x\"";
                                            // set current month selection to previously set month
                                            if ($x == $month) {
                                                echo " SELECTED";                            
                                            }
                                            // setting displayed text in selection box to month word value from array $months
                                            echo ">".$months[$x-1]."";
                                        }                
                                    ?>
                                </select>
                                <select name="year">
                                    <?php
                                        // create a list of option for a range of years
                                        for ($x=2015; $x <= 2099; $x++) {
                                            // ption value may be skipped because is numebric and it will be assigned anyway from number coming from $x
                                            echo "<option";
                                            // option is selected if current iterator is equal the year set by top script and prev form selection
                                            if ($x == $year) {
                                                echo " SELECTED";                            
                                            }         
                                            // numeric value as year to display in selection drop down menu
                                            echo ">$x";
                                        }
                                    ?>
                                </select>
                                <button type="submit" class="btn btn-default">Load</button
                            </form>
                            </center>
                        </div>
                    </div>   
                    <br>
                    <center>
                        <a href="index.php"><button type="button" class="btn btn-default">Home Page</button></a><br><br>                    
                    </center>
                      
                </div>
            </div>
        </div> <!-- wrapper -->        
        
        <!-- scripts -->
        
        <!-- jQuery -->
        <script src="js/jquery-1.11.1.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>        
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>        
        
    </body>
</html>

