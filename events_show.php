<html>
    <head>
        <title>Show/Add Events</title>
    </head>
    <body>
<?php
    // include connection to mysql database
    include('spsoc_db_conn.php'); 
    
    // first time event_1.php is called it called with $_GET variables
    // we must check are they set and if not set them to default invalid number 0 for day, month, year
    $iniMonth = isset($_GET['m']) ? $_GET['m'] : 0;
    $iniDay = isset($_GET['d']) ? $_GET['d'] : 0;
    $iniYear = isset($_GET['y']) ? $_GET['y'] : 0;   
    
    // because add event form is using a POST method and form action  is script itself
    // on self call variables $_GET are not set so will be all 0 as default
    // therefore we must perform a check is that so
    // and then check form $_POST variables are they set by the form. If not same default are applied
    if ($iniMonth == 0 && $iniDay == 0 && $iniYear == 0) {
        $iniMonth = isset($_POST['m']) ? $_POST['m'] : 0;
        $iniDay = isset($_POST['d']) ? $_POST['d'] : 0;
        $iniYear = isset($_POST['y']) ? $_POST['y'] : 0;           
    }
    
    // line important to set temp variable if $_POST operation variable is not set
    $iniOperation = isset($_POST['operation']) ? $_POST['operation'] : "no_insert";
    
    // showing events for this day    
    
    // prepared sql statement getting event for a given by $_GET date
    // check carefully column names because wrong one cause a query to corrupt whole script
    $query = "SELECT event_title, event_short_desc, date_format(event_start, '%l:%i %p') as fmt_date FROM calendar_events WHERE month(event_start) = ? AND dayofmonth(event_start) = ? AND year(event_start) = ? ORDER BY event_start";
 
    $event_txt = "";   
    
    if ($stmt = mysqli_prepare($conn, $query)) {

        /* bind parameters for markers */
        mysqli_stmt_bind_param($stmt, 'sss', $iniMonth, $iniDay, $iniYear);         

        /* execute statement */
        mysqli_stmt_execute($stmt);

        /* bind result variables */
        mysqli_stmt_bind_result($stmt, $event_title, $event_short_desc, $fmt_date);

        
        /* store result must be executed to determine number of rows */
        mysqli_stmt_store_result($stmt);

        //printf("Number of rows: %d.\n", mysqli_stmt_num_rows($stmt));        
        
        if (mysqli_stmt_num_rows($stmt)>0) {        
        
            /* fetch values */
            while (mysqli_stmt_fetch($stmt)) {
                $event_txt .= "<P><strong>$fmt_date</strong>:$event_title<br>$event_short_desc";
            }

        }
        /* close statement */
        mysqli_stmt_close($stmt);
    }   
    
    if ($event_txt != "") {
        echo "<P><strong>Today's Events:</strong>$event_txt<hr noshade width=80%>";
    }
    
    /* close connection */
    mysqli_close($conn);    
?>
        <a href="events_calendar.php">Back to events calendar</a>
    </body>
</html>