<?php
?>

<html>
    <head>
        
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="DkIT Sports and Societies">
    <meta name="author" content="Lech Jankowski">        
        
        <title>DkIT Sport & Societies: Social Media</title>
        
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/spsoc.css" rel="stylesheet">
        
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">        

        <script src="js/bootstrap.min.js"></script>        
               
    </head>

    <body>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>        
        <div id="wrapper">
        
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">DkIT Sport & Societies</a>
                </div>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">

                    <ul class="nav navbar-nav side-nav">
                        <li><a href="bs_info.php">About</a></li>                        
                        <li><a href="bs_spsoc_list.php">List Sports & Societies</a></li>
                        <li><a href="bs_events_calendar.php">Events Calendar</a></li>
                        <li class="active"><a href="bs_social.php">Social Media</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->                    
            </nav>            
            
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="img/DkITS&S_300x173.png" class="img-responsive" alt="DKIT Sports and Societies logo">
                            <center>

                            <div class="fb-page" data-href="https://www.facebook.com/pages/DkIT-Sports-and-Societies/193183837423620" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true">
                                <div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote>
                                </div>
                            </div>                            
                            </center>
                        </div>
                    </div>       
                    <br><br>
                    <center>
                        <a href="index.php"><button type="""button" class="btn btn-default">Home Page</button></a><br><br>
                    </center>                     
                </div>
            </div>
            <!-- /#page-content-wrapper -->            
            
        </div> <!-- wrapper -->
        
        <!-- jQuery -->
        <script src="js/jquery-1.11.1.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>       
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>
    
    </body>
</html>

