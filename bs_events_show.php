<?php
    // include connection to mysql database
    include('spsoc_db_conn.php'); 

    // first time event_1.php is called it called with $_GET variables
    // we must check are they set and if not set them to default invalid number 0 for day, month, year
    $iniMonth = isset($_GET['m']) ? $_GET['m'] : 0;
    $iniDay = isset($_GET['d']) ? $_GET['d'] : 0;
    $iniYear = isset($_GET['y']) ? $_GET['y'] : 0;   

    // because add event form is using a POST method and form action  is script itself
    // on self call variables $_GET are not set so will be all 0 as default
    // therefore we must perform a check is that so
    // and then check form $_POST variables are they set by the form. If not same default are applied
    if ($iniMonth == 0 && $iniDay == 0 && $iniYear == 0) {
        $iniMonth = isset($_POST['m']) ? $_POST['m'] : 0;
        $iniDay = isset($_POST['d']) ? $_POST['d'] : 0;
        $iniYear = isset($_POST['y']) ? $_POST['y'] : 0;           
    }
    $iniDateStr = "".$iniDay."/".$iniMonth."/".$iniYear;
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="DkIT Sports and Societies">
        <meta name="author" content="Lech Jankowski">
        
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/spsoc.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">        

        <script src="js/bootstrap.min.js"></script>  
        
        <title>DkIT Sports&Societies Events</title>
    </head>
    <body>
       <div id="wrapper">
        
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">DkIT Sport & Societies</a>
                </div>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">

                    <ul class="nav navbar-nav side-nav">
                        <li><a href="bs_info.php">About</a></li>                        
                        <li><a href="bs_spsoc_list.php">List Sports & Societies</a></li>
                        <li><a href="bs_events_calendar.php">Events Calendar</a></li>
                        <li><a href="bs_social.php">Social Media</a></li>                        
                    </ul>
                </div>
                <!-- /.navbar-collapse -->                    
            </nav>  
            
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">         
                            <img src="img/DkITS&S_173x100.png" class="img-responsive" alt="DKIT Sports and Societies logo">
                            
                            <!-- displaying a breadcrumb -->
                            <ul class="breadcrumb">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="bs_events_calendar.php">Calendar of events</a></li>
                                <li class="active"><a href="bs_events_show.php?m=<?php echo $iniMonth."&d=".$iniDay."&y=".$iniYear ?>">Calendar of events for <?php echo $iniDateStr;?></a></li>
                            </ul>    
                        </div>
                    </div>                      
                    <div class="row">
                        <div class="col-lg-12">          
                            <?php
                                // line important to set temp variable if $_POST operation variable is not set
                                $iniOperation = isset($_POST['operation']) ? $_POST['operation'] : "no_insert";

                                // showing events for this day    

                                // prepared sql statement getting event for a given by $_GET date
                                // check carefully column names because wrong one cause a query to corrupt whole script
                                $query = "SELECT event_title, event_short_desc, date_format(event_start, '%l:%i %p') as fmt_date FROM calendar_events WHERE month(event_start) = ? AND dayofmonth(event_start) = ? AND year(event_start) = ? ORDER BY event_start";

                                $event_txt = "";   

                                if ($stmt = mysqli_prepare($conn, $query)) {

                                    /* bind parameters for markers */
                                    mysqli_stmt_bind_param($stmt, 'sss', $iniMonth, $iniDay, $iniYear);         

                                    /* execute statement */
                                    mysqli_stmt_execute($stmt);

                                    /* bind result variables */
                                    mysqli_stmt_bind_result($stmt, $event_title, $event_short_desc, $fmt_date);


                                    /* store result must be executed to determine number of rows */
                                    mysqli_stmt_store_result($stmt);

                                    //printf("Number of rows: %d.\n", mysqli_stmt_num_rows($stmt));        

                                    if (mysqli_stmt_num_rows($stmt)>0) {        

                                        /* fetch values */
                                        while (mysqli_stmt_fetch($stmt)) {
                                            $event_txt .= "<P><strong>$fmt_date</strong>:$event_title<br>$event_short_desc";
                                        }

                                    }
                                    /* close statement */
                                    mysqli_stmt_close($stmt);
                                }   

                                if ($event_txt != "") {
                                    echo "<P><strong>Today's Events:</strong>$event_txt<hr noshade width=80%>";
                                }

                                /* close connection */
                                mysqli_close($conn);    
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <center>
            <a href="bs_events_calendar.php"><button type="button" class="btn btn-default">Back to events calendar</button></a><br><br>                    
        </center>
        
        <!-- scripts -->
        
        <!-- jQuery -->
        <script src="js/jquery-1.11.1.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>       
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>           
    </body>
</html>