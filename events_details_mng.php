<?php
    // include connection to mysql database
    include('spsoc_db_conn.php');   
    $searched_id = isset($_GET['id']) ? filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS) : 0;    
?>

<html>
    <head>
        <title>Event Details</title>
    </head>
    <body>
        <h2>Dundalk Institute of Technology</h2>
        <h2>Event Details for:</h2>

<?php

    //setting output to null
    $event_output_txt = "";

    // getting the tiltle of sports or societies from database
    
    // preparing the query
    $query = "SELECT * FROM calendar_events WHERE id=?";    
    
    if ($stmt = mysqli_prepare($conn, $query)) {

        /* bind parameters for markers */
        mysqli_stmt_bind_param($stmt, 's', $searched_id);         

        /* execute statement */
        mysqli_stmt_execute($stmt);

        /* bind result variables */
        mysqli_stmt_bind_result($stmt, $id, $event_title, $event_short_desc, $event_start);
                
        /* store result must be executed to determine number of rows */
        mysqli_stmt_store_result($stmt);
        
        if (mysqli_stmt_num_rows($stmt)>0) {        



            // setting initial value of event id. It must be used outside the loop to link to a proper page
            $event_id = 0;
            
            // fetching array of results and setting just an event_titles to html text $event_title_txt
            while (mysqli_stmt_fetch($stmt)) {

                // storing unique society id
                $event_id = stripslashes($id);

                // storing clean fields of sport or society
                $event_title = stripslashes($event_title);
                $event_short_desc = stripslashes($event_short_desc);
                $event_start = stripslashes($event_start);

                // building up output html list
                $event_output_txt .= "<h2><strong>$event_title</strong></h2><br><br>\n"
                        . "<p><strong>Short description:&nbsp;</strong>$event_short_desc</p>"
                        . "<p><strong>Start date/time:&nbsp;</strong>$event_start</p>";
                }
        }
        /* close statement */
        mysqli_stmt_close($stmt);
    }      
   
    echo $event_output_txt;

    /* close connection */
    mysqli_close($conn);     
    
?>
        <br><br>

        <a href="events_edit.php?id=<?php echo $event_id; ?>">Edit entry</a> <br>
        <a href="events_delete.php?id=<?php echo $event_id; ?>">Delete entry</a> <br><br>       

        <a href="events_calendar_mng.php">Click to return to the Sport and Societies Events Calendar Management</a> <br>
    </body>
</html>